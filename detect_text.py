import io
import os

from google.cloud import vision
from google.protobuf.json_format import MessageToJson

import json

from PIL import Image, ImageDraw, ImageFont
import numpy as np

import logging
logging.basicConfig(level=logging.INFO)

def detect_text(path, out):
    """Detects text in the file."""

    assert out[-4:] == 'json', 'output extension must be json'
    
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)
    
    # 日本語、英語ヒント
    # image_context = vision.types.ImageContext(language_hints=['ja', 'en'])
    # response = client.document_text_detection(image=image, image_context=image_context)
    
    response = client.text_detection(image=image)

    # json形式に変換
    serialized = MessageToJson(response)

    # 保存
    fw = open('{}'.format(out), 'w')
    print('{}'.format(serialized), file=fw)

def draw_annotation(image, text, rec, color=(157, 204, 224), draw_label=True):
    # 日本語フォントの利用
    # http://mplus-fonts.osdn.jp/about.html#download-1
    font = 'mplus-1c-medium.ttf'
    
    font = ImageFont.truetype(font=font,size=np.floor(3e-2 * image.size[1] + 0.5).astype('int32'))
    thickness = (image.size[0] + image.size[1]) // 300
        
    draw = ImageDraw.Draw(image)
    
    label_size = draw.textsize(text, font)
    
    left,top, right, bottom = rec
    
    top = max(0, np.floor(top + 0.5).astype('int32'))
    left = max(0, np.floor(left + 0.5).astype('int32'))
    bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
    right = min(image.size[0], np.floor(right + 0.5).astype('int32'))

    if top - label_size[1] >= 0:
        text_origin  = np.array([left, top - label_size[1]])
    else:
        text_origin  = np.array([left, top + 1])

    for i in range(thickness):
        draw.rectangle(
            [left + i, top + i, right - i, bottom - i],
            outline=color)
    
    if draw_label:
        draw.rectangle([tuple(text_origin), tuple(text_origin + label_size)],fill=color)
        draw.text(text_origin, text, fill=(0, 0, 0), font=font)

    return image

def save_annotation_image(image_path, annotation, outdir, draw_per_annotation=True):
    from copy import copy

    os.makedirs(outdir, exist_ok=True)

    if draw_per_annotation:
        sub_outdir = os.path.join(outdir, os.path.basename(image_path).split('.')[0])
        os.makedirs(sub_outdir, exist_ok=True)

    orig_img = Image.open(image_path).convert('RGB')

    with open(annotation) as f:
        lines = f.read()
    json_str = json.loads(lines)

    texts = json_str['textAnnotations']

    total_anno_img = copy(orig_img)

    for idx, t in enumerate(texts[1:]):
        logging.debug(t['description'])

        # 左上の点から時計回り
        # vertices = (left, top), (right, top), (right, bottom), (left, bottom)
        p0, p1 = t['boundingPoly']['vertices'][0], t['boundingPoly']['vertices'][2]
        logging.debug('left {} top {}'.format(p0['x'], p0['y']))
        logging.debug('right {} bottom {}'.format(p1['x'], p1['y']))

        img = copy(orig_img)
        rec = [p0['x'], p0['y'],p1['x'], p1['y']]

        # 対象を矩形でアノテーション
        draw_img = draw_annotation(img, t['description'], rec)
        
        total_anno_img = draw_annotation(total_anno_img, t['description'], rec, draw_label=False)
        
        
        draw_img.save(os.path.join(sub_outdir, '{}.png'.format(idx)))
        
    total_anno_img.save(os.path.join(outdir, '{}'.format(os.path.basename(image_path))))


if __name__ == '__main__':
    image_path = '621783.png'
    out = 'out'
    annotation = '621783.json'
    # detect_text(path, out)
    
    save_annotation_image(image_path, annotation, out)